if __name__=="__main__":

    # 2 Methods to print with python

    name = "Sara"
    age = 21
    #birthday = 19/02/1999
    day = 19
    month = "02"
    year = 1999

    # First method
    print("My name is %s and I'm %d years old, I was born in %d/%s/%d \n" %(name, age, day, month, year))
    # Second method    
    print(f"My name is {name} and I'm {age} years old and")
    print(f"I was born the {day}/{month}/{year}")