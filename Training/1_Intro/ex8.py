if __name__=="__main__":

    #First method
    # we could create a dictionary with all the keys and the emtpy values and then fill it
    personal_data= {
        "projectName": "",
        "company": "",
        "deviceList":[
            {
                "deviceID": "",
                "deviceName": "",
                "deviceType": "",
            }
        ]}

    personal_data['projectName']= input("Write the project name: ")
    personal_data['company']= input("Write the name of the company: ")
    personal_data['deviceList'][0]['deviceID']= input("Write the device ID: ")
    personal_data['deviceList'][0]['deviceName']= input("Write the device name: ")
    personal_data['deviceList'][0]['deviceType']= input("Write the device type: ")

    print(personal_data)

    #Second method
    # We will create an empty dictionary to store everything and then add keys and values one by one
    personal_data={}
    personal_data['projectName']= input("Write the project name: ")
    personal_data['company']= input("Write the name of the company: ")
    #I have to create an empty list to store the devices
    personal_data['deviceList']=[]
    #I have to create a dictionary to store the device data
    device={
        "deviceID": "",
        "deviceName": "",
        "deviceType": "",
    }
    device["deviceID"]= input("Write the device ID: ")
    device["deviceName"]= input("Write the device name: ")
    device["deviceType"]= input("Write the device type: ")
    #I have to add the device to the list
    personal_data['deviceList'].append(device)

    print(personal_data)


