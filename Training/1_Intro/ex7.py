if __name__=="__main__":

    #Script to calculate the average, the max and the min of a list of number
    numbers= [1, 2, 3, 4, 5]
    #initialization of min, max and avg
    min = 10**5
    max = -10**5
    average= 0

    for item in numbers:
        average += item
        if item<min :
            min=item
        elif item>max:
            max=item

    average= average/len(numbers)

    print(f"Min: {min}")
    print(f"Max: {max}")
    print(f"Average: {average}")