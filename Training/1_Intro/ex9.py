import json
if __name__=="__main__":  
    
    #Save the variable personal_data from ex8 into a json file

    personal_data= {
        "projectName": "",
        "company": "",
        "deviceList":[
            {
                "deviceID": "",
                "deviceName": "",
                "deviceType": "",
            }
        ]}

    personal_data['projectName']= input("Write the project name: ")
    personal_data['company']= input("Write the name of the company: ")
    personal_data['deviceList'][0]['deviceID']= input("Write the device ID: ")
    personal_data['deviceList'][0]['deviceName']= input("Write the device name: ")
    personal_data['deviceList'][0]['deviceType']= input("Write the device type: ")
    # We save personal_data in a json file
    json.dump(personal_data, open("personalData.json",'w'))