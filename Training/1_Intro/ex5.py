if __name__=="__main__":

    #How to work on file

    #Opens the first file (original.txt)
    f1= open("original.txt")
    #Shows its content
    file1content = f1.read()
    print(file1content)
    f1.close()

    #sentence to add
    sentence_to_add= 'This is the copy of the original file:'
    #Write the content of original.txt in copy.txt
    f2= open("copy.txt", 'w')
    f2.write(sentence_to_add + '\n' + file1content)
    #Check if the operation was succesfull by printing the content of copy
    f2.close()
    f2=open("copy.txt")
    file2content = f2.read()
    print(file2content)
    f2.close()