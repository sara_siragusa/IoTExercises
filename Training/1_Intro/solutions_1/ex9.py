import json
if __name__ == "__main__":
    personal_data = {}
    personal_data['projectName'] = input("Write the project name: ")
    personal_data['company'] = input("Write the company name: ")
    # We create an empty list to store the devices
    personal_data['deviceList'] = []
    # We create a dictionary to store the device data
    device = {
        "deviceID": "",
        "deviceName": "",
        "deviceType": "",
    }
    device['deviceID'] = input("Write the device ID: ")
    device['deviceName'] = input("Write the device name: ")
    device['deviceType'] = input("Write the device type: ")
    # We add the device to the list
    personal_data['deviceList'].append(device)
    # We save personal_data in a json file
    json.dump(personal_data, open("personal_data.json", "w"))


