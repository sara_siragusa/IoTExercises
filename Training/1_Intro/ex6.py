if __name__=="__main__":

    #Script to check wheter a number it's a multiple of 2 or 3

    number= int(input("Please, insert a number different from 0 here: "))
    if number==0 :
        print("Warning! \n You inserted 0 as number, try again next time!")
    elif number%2==0 and number%3==0 :
        print(f" {number} is divisible by both 2 and 3")
    elif number%2==0:
        print(f"{number} is a multiple of 2")
    elif number%3==0 :
        print(f"{number} is a multiple of 3")
    else:
        print(f" {number} is not multiple of neither 2 nor 3")
