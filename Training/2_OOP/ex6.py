#Statistics about NBA players

import json
 
class NBAStats():
    def __init__(self,filename):
        self.filename = filename
        self.file_content= json.load(open(filename))  
    
    #def get_players(self):
    #    return self.file_content['players']
    
    def average_height(self):
        players= self.file_content['players']
        #players= get_players(self.file_content)
        for player in players:
            heights= player['hgt']/39.37     #conversion from inches in meters
        return sum(heights)/len(heights)
    
    def average_weight(self):
        players= self.file_content['players']
        #players= get_players(self.file_content)
        weights= [player['weight']/2.205 for player in players]   #conversion from pounds to kilograms
        return sum(weights)/len(weights)
    
    def average_ratings(self):
        players= self.file_content['players']
        #players= get_players(self.file_content)
        num_players= len(players)
        avg_ratings= dict.fromkeys(players[0]['ratings'][0],0) #inizialization; The fromkeys() method returns a dictionary with the specified keys and the specified value
        for player in players:
            player_ratings= player['ratings'][0]
            for key in player_ratings.keys():
                avg_ratings[key]+= avg_ratings[key]/num_players
        return avg_ratings
    
    def average_age(self):
        players= self.file_content['players']
        #players=get_players()
        ages=[2020-player['born']['year'] for player in players]
        return sum(ages)/len(ages)
    
if __name__ == '__main__':
    nba= NBAStats('playerNBA.json')
    print(f'Average height: {nba.average_height}')
    print(f'Average weight: {nba.average_weight}')
    print(f'Average ratings: {nba.average_ratings}')
    print(f'Average age: {nba.average_age}')   
