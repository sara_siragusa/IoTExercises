#I want to read some contacts that are stored in the file contacts.json

import json

class Contact():
    """Each contact is defined by his name surname and mail"""
    def __init__(self, name, surname, mail):
        self.name = name
        self.surname = surname
        self.mail = mail
    def __repr__(self):
        return "{},{},{}".format(self.name,self.surname,self.mail)
        
class ContactManager():
    def __init__(self,fileName):
        self.fileName= fileName
        self.contactsList=[]
    #I want to define a function (method) that loads all the contacts inside a contact list of this contactManager
    def loadContacts(self):
        fp=open(self.fileName)  #pointer to the file; I open the file in reading mode
        td=json.load(fp)        #temporary dictionary; it's just a place to store the file (the content of the json) for the moment, until we read the contacts and put them inside our list
        #print(td)       
        #for each contact in the temporary dictionary I want to create a new dictionary inside the contact list
        for contact in td["contacts"]:    #scorro tutti i contatti che trovo nel file che ho caricato su td
            #we create a new contact that has 3 fields
            newContact=(
                {"name": contact["name"], 
                 "surname": contact["surname"], 
                 "mail": contact["mail"],
                }
            )
            #we just created a new contact, now we have to add it to self.contactList
            self.contactsList.append(newContact)
    #This method will print each contact    
    def showContacts(self):
        for contact in self.contactsList:
            print(contact)


if __name__=="__main__":
    cm=ContactManager("contacts.json")
    #if the file is in another folder I need to give the filename taking into account another folder
    #cm=ContactManager("../nameofthefolder/contacts.json")
    cm.loadContacts()
    cm.showContacts()

    #This is how to save all the contacts we are running inside the program in a file
   # json.dump(prova, open("contacts.json",'w'))