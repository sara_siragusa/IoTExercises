#I want to implement a "Point" class that is able to represent 2-D points

import math

class Point():
    def __init__(self,x,y):
        self.x= x
        self.y= y
    def distance(self, other):
        return math.sqrt((self.x-other.x)**2+(self.y-other.y)**2)
    def move(self,dx,dy):
        self.x += dx
        self.y += dy
        return f"({self.x},{self.y})"

    # Optional
	# this method is executed when we ask to print an oject of this class
	# we need to return the string representation of the object
	# as P: (x,y)
	# we can use the repr method to do this
    def __repr__(self):
	    return f"P: {self.x}, {self.y}"
    #If I don't use this method it will print the memory address of the object

if __name__=="__main__":
    #create 2 point and calculate distance among them
    P1= Point(8,3)
    P2= Point(4,6)
    print(f"Distance between P1 and P2 is : {P1.distance(P2)}")

    #move P1 point according to vector (dx,dy)
    P1.move(2,2)
    print(P1)
