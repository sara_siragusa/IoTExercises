import json
from ex5 import *

if __name__ == '__main__':
    book = AddressBook('contacts.json')
    print('Welcome to the application to manage your contacts')
    helpMessage= ("\nPress 's' to show the list of contacts\n\
    Press 'n' to add a new contact\n\
    Press 'u' to update a contact\n\
    Press 'fn' to find a contact by name\n\
    Press 'd' to delete a contact\n\
    Press 'q' to save all changes and quit")
    
    while True:    #significa "gira all'infinito", infatti usciamo con un break se viene premuto 'q'
        print(helpMessage)
        command= input('Your selection: ')
        
        if command=='s':
            book.show()
        elif command=='n':
            name= input('Write the name of the contact: ')
            surname= input('Write the surname of the contact: ')
            mail= input('Write the mail of the contact: ')
            book.add_contact(name, surname, mail)
        elif command=='u':
            name= input('Write the name of the contact: ')
            surname= input('Write the surname of the contact: ')
            book.update_mail(name, surname)
        elif command=='fn':
            name=input('Write the name of the contact: ')
            book.find_by_name(name)
        elif command=='d':
            name=input('Write the name of the contact: ')
            book.remove_contact(name)
        elif command=='q':
            book.save()
            break
        else:
            print('Command not available')
            