#Deck of cards        
import random

suits= ['Hearts','Diamonds','Clubs','Spades']
values= ['A','2','3','4','5','6','7','8','9','10','J','Q','K']

class Card(object):
    """Ogni carta ha un valore ed un seme associato quando viene creata"""
    def __init__(self, suit, value):
        self.suit= suit
        self.value= value
        
    def __repr__(self):
        return f"{self.value} of {self.suit}"
    
class Deck(object):
    """Il mazzo (Deck) contiene 13 carte per ciascuno dei 4 semi
    I metodi dell'oggetto sono:
        -il metodo shuffle() che restituisce il mazzo mescolato
        -il metodo draw(n) che restituisce una lista di n carte estratte dal mazzo
    """
    
    def __init__(self):
        #riempio il mazzo con le carte      
        self.cards = [ Card(s,v) for s in suits for v in values]
        
    def shuffle(self):
        """Mescola il mazzo"""
        random.shuffle(self.cards)
        
    def draw(self, n=1):
        """Estrae n carte dal mazzo, n=1 se non specificato

        Args:
            n (int, optional): [numero di carte da estrarre]. Defaults to 1.
        """
        #Controllo che n sia minore del numero di carte rimaste nel mazzo
        #Se è così estraggo n carte
        if n<len(self.cards):
            drawn=[]           #carte estratte
            for i in range(n):
                drawn.append(self.cards.pop())   #rimuovo una carta dal mazzo e la metto nelle carte estratte
            return drawn
        #Se n è maggiore del numero di carte rimanenti nel mazzo, resituisco tutte le carte che rimangono
        elif n>len(self.cards) and len(self.cards)>0:    #controllo che ci siano ancora carte nel deck
            #il numero di carte da resituire è uguale al numero di carte rimaste in deck (len(self.cards))
            #metodo 1
            drawn=[]
            for i in range(len(self.cards)):
                drawn.append(self.cards.pop())
            return drawn
            #metodo 2) richiamo il metodo draw e imposto come n il numero di carte rimaste nel deck
            #return self.draw(len(self.cards))
        else:
            return None
        

