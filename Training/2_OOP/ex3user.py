#This program allows an user to draw some cards from a Deck; it is alos possible to shuffle the Deck

from ex3 import *

if __name__ == '__main__':
    
    deck= Deck()
    #I want to shuffle the Deck for the first time, so that the users can draw some cards from an already shuffled deck
    deck.shuffle()
    while True:   #This allows the user to shuffle the deck or draw cards until the Deck is empty    
        user_input= input("What do you want to do?\nS: Shuffle the deck \nD: Draw some cards \nQ: Quit \n")
        if user_input=='S':
            deck.shuffle()
            pass
        elif user_input=='D':
            n= int(input("How many cards do you want to draw? "))
            print(f"Cards drawn: {deck.draw(n)}")
            pass
        elif user_input=='Q':
            break
        else:
            print("Command not recognized")
    print("Goodbye")