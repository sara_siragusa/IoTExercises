#I want to create a square manager class, that is able to evaluate 
# the perimeter the area and the diagonal of a square 
# knowing the length of his side

import math

#To define a class in python we nedd to write class <nameoftheclass>
class SquareManager():
    #the first thing we need to put in every class that we define is the __init__ method
    def __init__(self, side):
        #init method always takes as input self and all the parameters that we want to give to the object at the beginning
        self.side = side     #I'm telling the square "your own side will be equal to side"
    #Now we need to create the methods for the class
    def area(self):     
        return self.side**2
    def perimeter(self):
        return self.side*4
    def diagonal(self):
        return self.side*(math.sqrt(2))

if __name__=="__main__":
    sm=SquareManager(9)     
    print(f"The area of the square with side {sm.side} = {sm.area()}")     
    print(f"The perimeter of the square with side {sm.side} = {sm.perimeter()}")     
    print(f"The diagonal of the square with side {sm.side} = {sm.diagonal():.3f}")

    