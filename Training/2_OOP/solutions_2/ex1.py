import math 
class Point:
	def __init__(self,x,y):
		self.x = x
		self.y = y
	def distance(self,other):
		return math.sqrt((self.x-other.x)**2+(self.y-other.y)**2)
	def move(self,dx,dy):
		self.x += dx
		self.y += dy
		return f"({self.x}, {self.y})"

	# Optional
	# this method is executed when we ask to print an oject of this class
	# we need to return the string representation of the object
	# as P: (x,y)
	# we can use the repr method to do this
	def __repr__(self):
		return f"P: {self.x}, {self.y}"


if __name__=="__main__":
	a=Point(3,4)
	b=Point(5,6)
	print(a.distance(b))
	print(a.move(1,1))
	print(a)