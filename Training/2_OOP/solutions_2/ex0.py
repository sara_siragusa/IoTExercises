import math
class SquareManager:
	def __init__(self,sideLenght):
		self.sideLenght = sideLenght
	def area(self):
		return self.sideLenght**2
	def perimeter(self):
		return self.sideLenght*4
	def diagonal(self,root):
		return self.sideLenght*root

if __name__=="__main__":
	square = SquareManager(5)
	ws=SquareManager(10)
	print(square.area())
	print(square.perimeter())
	print(square.diagonal(math.sqrt(2)))
	print(square.sideLenght)