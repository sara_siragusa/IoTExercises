#Creare un AdressBook con dei metodi che mi consentano di: mostrare i contatti, trovare un contatto, rimuovere un contatto (dal nome), aggiungere un contatto
#CRUD: Create, Read, Update, Delete

import json

class Contact():
    def __init__(self, name, surname, mail):
        self.name= name
        self.surname= surname
        self.mail = mail       
    def __repr__(self):
        return "{}, {}, {}".format(self.name, self.surname, self.mail)
    
class AddressBook():
    def __init__(self,fileName):    #Inizializzo l'oggetto AddressBook indicandogli il file dove sono memorizzate le informazioni sui contatti
        #Di cosa ho bisogno?
        self.fileName= fileName     #sapere dove devo andare a prendere i contatti
        self.contactList= []        #inizializzare una lista contatti dove andrò a mette il contenuto del json che devo ancora leggere
        #Adesso posso leggere il contenuto del json e metterlo su un temporary dictionary
        fp= open(fileName)
        td= json.load(fp)
        #Sposto il contenuto del td sulla lista contatti del mio AddressBook
        for contact in td["contacts"]:   #per capire perchè ho indicizzato così td andare a vedere come è fatto il file contacts.json
            #in questo momento sto leggendo dal dizionario un contatto alla volta; ciascun contatto sarà a sua volta un dizionario
            #Creo un oggetto contatto passandogli come attributi i campi del dizionario-contatto appena letto
            #"attacco" il nuovo contatto alla lista-contatti dell'address-book
            self.contactList.append(Contact(contact["name"], contact["surname"], contact["mail"]))
    
    #READ: Funzione (metodo dell'oggetto AddressBook) per stampare a video tutti i contatti        
    def show(self):
        for contact in self.contactList:  #nota che per richiamare un attributo dell'oggeto devi specificare "self." prima
            print(contact)
    
    #Funzione che aggiunge il contatto alla lista-contatti
    def add_contact(self, name, surname, mail):
        self.contactList.append(Contact(name, surname, mail))
    
    #Funzione che mi permette di aggiornare la mail di un contatto che abbia un certo nome e cognome
    def update_mail(self, name, surname):
        updated= False       #flag che mi da la condizione di uscita dal ciclo while: se ho aggiornato la mail esco dal ciclo
        i=0
        while not updated:
            if self.contactList[i].name == name and self.contactList[i].surname == surname:
                self.contact[i].mail= input(f"Insert the new mail of {name} {surname}: ")
                updated= True  #esco dal ciclo
            i=i+1   #se non ho aggiornato passo al contatto successivo
       
    #Funzione che restituisce tutti i contatti che hanno un certo nome
    def find_by_name(self, name):       #devo passargli in input il nome da cercare
        #Ho bisogno di una lista dove mettere i risultati della ricerca
        #OPZIONE 1 
    #    results= []
    #    for contact in self.contactList:
    #        if contact.name == name:        #ricorda che contactList contiene degli oggetti Contact, ciascuno dei quali avrà il suo attributo "name"
    #            results.append(contact)
        #OPZIONE 2
        results= [contact for contact in self.contactList if contact.name==name]
        print("I found the following results: \n")
        for x in results:
            print(x)
    
    #Funzione che elimina dall'AddressBook tutti i contatti che hanno il nome indicato        
    def remove_contact(self, name):
        #controllo ciascun contatto e se ha quel nome lo rimuovo dalla lista contatti
        for i in range(len(self.contactList)):
            if self.contactList[i].name == name:
                self.contactList.pop(i)
     
    #Funzione che mi permette di salvare le modifiche fatte all'AddressBook    
    def save(self):
        jsonContent= {"contacts": [c.__dict__ for c in self.contactList]}
        #__dict__ serve a prendere gli attributi dell'oggetto
        json.dump(jsonContent, open(self.fileName, 'w'))
        
        
        
            
    

            
        
