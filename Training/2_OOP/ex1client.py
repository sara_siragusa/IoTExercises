#I want to create a client for the ex1
#I want to write a script that asks the user to insert points and calculates the distance between the points
from ex1 import *

if __name__=="__main__":
    while True:   #means keep running forever
        command=input("The available commands are:\n\
    d: for distance among 2 points\n\
    m: to move a point according to a vector\n\
    q: Quit\n")

        if command=="d":
            #I ask the user to give me the coordinates of the 2 points
            x1=int(input("x1: ")) 
            y1=int(input("y1: "))
            x2=int(input("x2: "))
            y2=int(input("y2: "))
            #I create the points
            P1= Point(x1,y1)
            P2= Point(x2,y2)
            #I calculate and give back to the user the distance among 2 points
            print(f"Distance: ",{P1.distance(P2)})
            pass
        elif command=="m":
            #I ask the user to give me the coordinates of the 2 points
            x1=int(input("x1: ")) 
            y1=int(input("y1: "))
            dx=int(input("dx: "))
            dy=int(input("dy: "))
            #I create the point
            P1= Point(x1,y1)
            #I move the point 1 within the value dx and dy
            print(P1.move(dx,dy))
            pass
        elif command=="q":
            break
